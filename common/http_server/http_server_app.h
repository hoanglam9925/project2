#ifndef __HTTP_SERVER_APP_H_
#define __HTTP_SERVER_APP_H_

typedef void (*http_post_callback_t) (char *data, int len);
// typedef void (*http_get_callback_t) (void); 
// typedef void (*http_get_data_callback_t) (char *data, int len); 
void stop_webserver(void);
void start_webserver(void);

void http_set_callback_wifi(void *cb);

#endif