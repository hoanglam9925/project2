#ifndef _WIFI_SOFT_AP_H_
#define _WIFI_SOFT_AP_H_

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"

#include "lwip/err.h"
#include "lwip/sys.h"

void wifi_init_softap(void);
static void wifi_event_handler(void* arg, esp_event_base_t event_base,
                                    int32_t event_id, void* event_data);
void wifiAPStop(void);
void set_APPASS(char *pass);
void set_APSSID(char *ssid);

#endif