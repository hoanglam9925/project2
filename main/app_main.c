/* MQTT Mutual Authentication Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "lwip/sockets.h"
#include "lwip/dns.h"
#include "lwip/netdb.h"

#include "esp_log.h"
#include "mqtt_client.h"

#include "app_config.h"
#include "esp_smartconfig.h"
#include "http_server_app.h"
#include "wifi.h"
#include "wifiSoftAP.h"
static const char *TAG = "MQTTS_EXAMPLE";

void wifi_callback(char *data, int len)
{
    char ssid[30] = "";
    char pass[30] = "";
    char *pt = strtok(data, "@");
    if(pt)
        strcpy(ssid, pt);
    pt = strtok(NULL, "@");
    if(pt)
        strcpy(pass, pt);
    printf("%s\n%s\n",ssid,pass);

    set_SSID(ssid);
    set_PASS(pass);
    wifi_mode_t temp;
    if(esp_wifi_get_mode(&temp) == ESP_OK)
    {
      if(temp == WIFI_MODE_STA)
      {
        stationStop();
        wifi_init_sta();
      }
      else if(temp == WIFI_MODE_AP)
      {
        wifiAPStop();
        wifi_init_sta();
      }
    }
}
void app_main(void)
{
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    http_set_callback_wifi(wifi_callback);
    start_webserver();
    app_config();
}
